#!/bin/bash
ENV_PASSWORD_SECRET=$1
ENV_TABLE_SECRET=$2

LIST_SECURITY_GROUP=$(aws ec2 describe-security-groups --filters '[{"Name":"vpc-id","Values":["'"$AWS_VPC_ID"'"]},{"Name":"group-name","Values":["'"$AWS_SECURITY_GROUP"'"]}]')

RESULTARRAY_LIST_SECURITY_GROUP=($LIST_SECURITY_GROUP)
echo 'resultado_ARRAY_LOG:' $RESULTARRAY_LIST_SECURITY_GROUP
PROPERTIES_LIST_SECURITY_GROUP=${RESULTARRAY_LIST_SECURITY_GROUP[2]}
echo 'resultado_PROPERTIES_LIST_SECURITY_GROUP:' $PROPERTIES_LIST_SECURITY_GROUP

if [ $PROPERTIES_LIST_SECURITY_GROUP == "[]" ]; then
	GROUP_ID=$(aws ec2 create-security-group --group-name $AWS_SECURITY_GROUP --description $AWS_SECURITY_GROUP --vpc-id $AWS_VPC_ID)
	ARRAY_GROUP_ID=($GROUP_ID)
	PROPERTY_GROUP_ID=${ARRAY_GROUP_ID[2]}
	echo 'ID del grupo' $PROPERTY_GROUP_ID
	PROPERTY_GROUP_ID_OK=$(echo $PROPERTY_GROUP_ID | sed 's/"//g')
	aws ec2 authorize-security-group-ingress --group-id $PROPERTY_GROUP_ID_OK --protocol tcp --port $AWS_CONTAINER_PORT --cidr $AWS_VPC_CIDR
else
    PROPERTY_GROUP_ID=${RESULTARRAY_LIST_SECURITY_GROUP[57]}
    echo 'resultado_PROPERTY_GROUP_ID:' $PROPERTY_GROUP_ID
    PROPERTY_GROUP_ID_OK=$(echo $PROPERTY_GROUP_ID | sed 's/"//g')
fi
RESULT_TARGET=$(aws elbv2 create-target-group --name $AWS_TARGET_GROUP --protocol TCP --port 80 --target-type ip --vpc-id $AWS_VPC_ID --health-check-protocol TCP --healthy-threshold-count 2 --unhealthy-threshold-count 2 --health-check-interval-seconds 10 --health-check-timeout-seconds 10)
RESULTARRAY_TARGET=($RESULT_TARGET)
PROPERTIES_TARGET=${RESULTARRAY_TARGET[21]}
PROPERTIES_TARGET_OK=$(echo $(echo $PROPERTIES_TARGET | sed 's/,//g') | sed 's/"//g')
echo 'PROPERTIES_TARGET_OK' $PROPERTIES_TARGET_OK

aws elbv2 create-listener --load-balancer-arn $AWS_NLB_ARN --protocol TCP --port $AWS_PORT_TARGET_NLB --default-actions Type=forward,TargetGroupArn=$PROPERTIES_TARGET_OK

RESULT_LOG=$(aws logs describe-log-groups --log-group-name-prefix /ecs/$AWS_TASK_DEFINITION)
echo 'resultado_LOG:' $RESULT_LOG
RESULTARRAY_LOG=($RESULT_LOG)
echo 'resultado_ARRAY_LOG:' $RESULTARRAY_LOG
PROPERTIES_LOG=${RESULTARRAY_LOG[2]}
echo 'resultado_PROPERTIES_LOG:' $PROPERTIES_LOG

if [ $PROPERTIES_LOG == "[]" ]; then
	echo "Array empty"
	echo "Creando el group log"
	aws logs create-log-group --log-group-name /ecs/$AWS_TASK_DEFINITION
else
	echo "Array non empty"
fi
#Variables de entorno
RESULT=$(aws ecs register-task-definition --family $AWS_TASK_DEFINITION --network-mode awsvpc --task-role-arn $AWS_TASK_ROLE_ARN --execution-role-arn $AWS_EXECUTION_ROLE_ARN --cpu $AWS_CPU --memory $AWS_MEMORY --requires-compatibilities "$AWS_REQUIRES_COMPATIBILITIES" --container-definitions '[{"name":"'"$AWS_NAME_CONTAINER"'","image":"'"$AWS_IMAGE"'","portMappings":[{"containerPort": '"$AWS_CONTAINER_PORT"',"hostPort": '"$AWS_HOST_PORT"',"protocol": "tcp"}],
"environment":[
	{"name":"SERVER","value":"'"$ENV_SERVER"'"},
	{"name":"PORT","value":"'"$ENV_PORT"'"},
	{"name":"DATABASE","value":"'"$ENV_DATABASE"'"},
	{"name":"USER","value":"'"$ENV_USER"'"},
	{"name":"PASSWORD","value":"'"$ENV_PASSWORD_SECRET"'"},
	{"name":"TABLE","value":"'"$ENV_TABLE_SECRET"'"}
],"logConfiguration": {"logDriver": "awslogs","options":{"awslogs-group":"/ecs/'"$AWS_TASK_DEFINITION"'","awslogs-region": "'"$AWS_ZONA"'","awslogs-stream-prefix": "ecs"}}}]')
echo 'resultado:' $RESULT

RESULTARRAY=($RESULT)
PROPERTIES=${RESULTARRAY[66]}}
DEFINITION=${PROPERTIES:52}
LEN=${#DEFINITION}
TASK=${DEFINITION::LEN-3}
echo $TASK

LISTA_SERVICIO=$(aws ecs list-services --cluster $AWS_CLUSTER)
echo 'resultado_LISTA_SERVICIO:' $LISTA_SERVICIO
if echo ${LISTA_SERVICIO[@]} | grep -q -w "$AWS_SERVICE_NAME"; then
	echo "Existe, se actualizará con el nuevo task definition"
	aws ecs update-service --service $AWS_SERVICE_NAME --cluster $AWS_CLUSTER --task-definition $TASK
else
	echo "No Existe. Creando servicio"
	aws ecs create-service --cluster $AWS_CLUSTER --service-name $AWS_SERVICE_NAME --launch-type FARGATE --task-definition $TASK --desired-count $AWS_NUMBER_TASK --deployment-configuration '{"maximumPercent":200,"minimumHealthyPercent":100}' --network-configuration '{"awsvpcConfiguration":{"subnets":['"$AWS_SUBNET_PRIVATE"'],"securityGroups":["'"$PROPERTY_GROUP_ID_OK"'"],"assignPublicIp":"'"$AWS_SERVICE_ASSIGN_PUBLIC_IP"'"}}' --load-balancers '[{"targetGroupArn":"'"$PROPERTIES_TARGET_OK"'","containerName":"'"$AWS_NAME_CONTAINER"'","containerPort":'"$AWS_CONTAINER_PORT"'}]'
fi