const dotenv = require('dotenv');
const mysql = require('mysql');
const express = require('express');
const bodyparser = require('body-parser');
const http = require('http');
const app = express();

app.use(bodyparser.json());
dotenv.config();

//MySQL details
var mysqlConnection = mysql.createConnection({
  host: process.env.SERVER,
  user: process.env.USER,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
  port: process.env.PORT
  });

mysqlConnection.connect((err)=> {
  console.log(process.env.SERVER);
  console.log(process.env.USER);
  console.log(process.env.PASSWORD);
  console.log(process.env.DATABASE);
  console.log(process.env.PORT);
  console.log(process.env.TABLE);
  if(!err)
    console.log('Connection Established Successfully.');
  else
    console.log('Connection Failed!'+ JSON.stringify(err,undefined,2));
});

app.get('/user', (req, res) => {
  mysqlConnection.query('SELECT * FROM user', (err, rows, fields) => {
    if (!err){
      console.log(rows);
      res.send(rows);
    }
    else{
      console.log(err);
    }    
  });
});
app.post('/user', (req, res) => {
  console.log("post user");
  res.status(202).send({message:'El usuario se está creando'});
});
app.put('/user', (req, res) => {
  console.log("put user");
  res.status(202).send({message:'El usuario se está actualizando'});
});
app.get('/health', (req, res) => {
  console.log("get health");
  res.status(200).send({message:'El microservicio se encuentra activo'});
});
http.createServer(app).listen(4000, () => {
  console.log('Server started at http://localhost:4000');
});